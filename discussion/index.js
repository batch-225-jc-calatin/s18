// Function Parameters and Arguments

// "name" is called a parameter that acts as a named/variable/container that exists only inside of the function

// "Nehimiah" - argument - it is the information/ data provided directly into the function.

function printInput (name){
	console.log("My Name is: " + name);

}
printInput("Nehemiah");


let sampleVariable = "yui";
printInput(sampleVariable);




function checkDivisibilityBy8(num){
				let remainder = num % 8;
				console.log("The remainder of " + num + " divided by 8 is: " + remainder);
				let isDivisibleBy8 = remainder === 0;
				console.log("Is " + num + " divisible by 8?");
				console.log(isDivisibleBy8);
			}

			checkDivisibilityBy8(64);
			checkDivisibilityBy8(28);



// Function Argument

// Function paramenters can also accept other functions as argument.

			function argumentFunction(){
				console.log("This function wass passed an argument before the message was printed")
			}

			function invokeFunction (argumentFunction){

				argumentFunction();
				console.log("This code comes from invokeFunction")

			};
			invokeFunction(argumentFunction);





			// Multiple arguments - will correspond to the number of "parameter" declared in a function in suceeding order

			function createFullName (firstName, middleName, lastName){
				console.log(firstName + ' ' + middleName + ' ' +lastName);
			}
			createFullName("Juan", "Dela");
			createFullName("Jane", "Dela", "Cruz");
			createFullName("Jane", "Dela", "Cruz", "Hello");

			let firstName = "john";
			let middleName = "Doe";
			let lastName ="Smith";
			createFullName(firstName, middleName, lastName);


			// The Return Statements - allows us to output a value from a function to be passed to the line/block of code that invoked or called the function

			// The return statement also stops the execution of the function and any code after the return statement will not be executed.

			function returnFullName (firstName, middleName, lastName) {


				return firstName + ' ' + middleName + ' ' + lastName;
				console.log("This Message will not be printed");
			}
			
			let completeName = returnFullName('joe', 'jayson', 'smith');
			console.log(completeName);


		function returnAddress (city, country) {
			let fullAddress = city + ", " + country;
			return fullAddress;
		}
		let myAddress = returnAddress("Cebu City", "Philippines");
		console.log(myAddress);



function getAverage(num1, num2, num3) {

	let average = (num1 + num2 + num3) / 3;

	console.log(average);
	
}

function printWords(getAverage) {
	console.log("This is your Average");

	getAverage(10, 20, 30);

	console.log("Thank you!");
}

printWords(getAverage);